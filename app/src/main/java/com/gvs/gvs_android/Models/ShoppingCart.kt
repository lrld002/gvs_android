package com.gvs.gvs_android.Models

class ShoppingCart {
    var id: String = ""
    lateinit var user: User

    var items: ArrayList<Item> = ArrayList()

    constructor() {

    }

    constructor(id: String, user: User, items: ArrayList<Item>) {
        this.id = id
        this.user = user
        this.items = items
    }


}
