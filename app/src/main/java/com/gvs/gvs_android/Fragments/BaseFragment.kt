package com.gvs.gvs_android.Fragments

import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.google.firebase.firestore.FirebaseFirestore


abstract class BaseFragment : Fragment() {

    val db = FirebaseFirestore.getInstance()

    abstract val tituloFragmento: String

    protected abstract fun setListeners()

    protected abstract fun initViews()
}
