package com.gvs.gvs_android.Activities

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import com.google.firebase.auth.FirebaseAuth
import com.gvs.gvs_android.App
import com.gvs.gvs_android.Models.User

class SplashActivity : BaseActivity() {

    //private lateinit var gvsLogo : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_splash)

        initViews()
        setListeners()

        if (FirebaseAuth.getInstance().currentUser != null) {
            nextActivity()
        } else {
            FirebaseAuth.getInstance().signInAnonymously()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val firebaseUser = FirebaseAuth.getInstance().currentUser
                        if (firebaseUser != null) {
                            val user = User(
                                firebaseUser.uid,
                                firebaseUser.email.orEmpty(),
                                "Anon",
                                "",
                                -1,
                                0,
                                false
                            )
                            db.collection(App.FIRESTORE_COLLECTION_USERS)
                                .document(user.id)
                                .set(user)
                                .addOnSuccessListener {
                                    nextActivity()
                                }
                                .addOnFailureListener { }
                        }
                    }
                }

        }
        finish()

    }

    private fun nextActivity() {
        val intent = Intent(this@SplashActivity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    override fun initViews() {
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    override fun setListeners() {
    }
}

