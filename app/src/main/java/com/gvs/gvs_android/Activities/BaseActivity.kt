package com.gvs.gvs_android.Activities

import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

abstract class BaseActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()

    internal abstract fun initViews()

    internal abstract fun setListeners()

}
