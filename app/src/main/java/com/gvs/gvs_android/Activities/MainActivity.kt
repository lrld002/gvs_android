package com.gvs.gvs_android.Activities

import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.gvs.gvs_android.Fragments.AccountFragment
import com.gvs.gvs_android.Fragments.HomeFragment
import com.gvs.gvs_android.Fragments.SearchFragment
import com.gvs.gvs_android.R

class MainActivity : BaseActivity() {

    private lateinit var toolbar: ActionBar

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                toolbar.title = "Home"
                val homeFragment = HomeFragment.newInstance()
                openFragment(homeFragment)
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_search -> {
                toolbar.title = "Search"
                val searchFragment = SearchFragment.newInstance()
                openFragment(searchFragment)
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_account -> {
                toolbar.title = "Account"
                val accountFragment = AccountFragment.newInstance()
                openFragment(accountFragment)
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        setListeners()
        openFragment(HomeFragment())

        toolbar = supportActionBar!!
        toolbar.title = "Home"
        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.Fragment_Container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun initViews() {
    }

    override fun setListeners() {

    }

}
