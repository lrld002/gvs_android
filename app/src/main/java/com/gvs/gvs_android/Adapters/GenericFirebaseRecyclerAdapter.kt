package com.gvs.gvs_android.Adapters

import android.content.Context
//import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions

abstract class GenericFirebaseRecyclerAdapter<T> constructor(
    val context: Context,
    private var mListener: OnViewHolderClick? = null,
    options: FirestoreRecyclerOptions<T>
) : FirestoreRecyclerAdapter<T, GenericFirebaseRecyclerAdapter.ViewHolder>(options) {

    interface OnViewHolderClick {
        fun onClick(view: View, position: Int)
    }

    class ViewHolder(view: View, private val mListener: OnViewHolderClick?) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        private val mMapView: MutableMap<Int, View>

        init {
            mMapView = HashMap()
            mMapView.put(0, view)

            if (mListener != null)
                view.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            mListener?.onClick(view, adapterPosition)
        }

        fun getView(id: Int): View {
            if (mMapView.containsKey(id))
                return mMapView[id]!!
            else
                initViewById(id)
            return mMapView[id]!!
        }

        private fun getView(): View = getView(0)

        private fun initViewById(id: Int) {
            val view = getView().findViewById<View>(id)
            if (view != null) {
                mMapView.put(id, view)
            }
        }

    }

    protected abstract fun createView(context: Context, viewGroup: ViewGroup, viewType: Int): View

    protected abstract fun bindView(item: T, position: Int, viewHolder: GenericFirebaseRecyclerAdapter.ViewHolder)

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): GenericFirebaseRecyclerAdapter.ViewHolder =
        GenericFirebaseRecyclerAdapter.ViewHolder(createView(context, viewGroup, viewType), mListener)

    override fun onBindViewHolder(holder: ViewHolder, position: Int, model: T) {
        bindView(getItem(position), position, holder)
    }

    override fun getItemCount(): Int {
        return snapshots.size
    }

    override fun getItem(position: Int): T {
        return this.snapshots[position]
    }

    fun setClickListener(listener: GenericFirebaseRecyclerAdapter.OnViewHolderClick) {
        mListener = listener
    }
}
